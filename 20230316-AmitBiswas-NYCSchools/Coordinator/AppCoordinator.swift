//
//  AppCoordinator.swift
//  20200704-AmitBiswas-NYCSchools
//
//  Created by Amit Biswas on 3/16/23.
//

import UIKit
import Foundation

class AppCoordinator {
    
    private let navigationController: UINavigationController
    private var schoolListCoordinator: SchoolListCoordinator?
    
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
        
}

//MARK: Helper Methods
extension AppCoordinator {
    
    func start() {
        self.schoolListCoordinator = SchoolListCoordinator(navigationController: self.navigationController)
        self.schoolListCoordinator?.start()
    }
    
}
