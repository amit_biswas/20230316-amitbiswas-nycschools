//
//  SchoolListCoordinator.swift
//  20200704-AmitBiswas-NYCSchools
//
//  Created by Amit Biswas on 3/16/23.
//

import Foundation
import UIKit

class SchoolListCoordinator {
    
    private let navigationController: UINavigationController
        
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    
    func start() {
        let schoolListVC = DataFactory.getSchoolsListViewController()
        schoolListVC?.delegate = self
        if let vc = schoolListVC {
            self.navigationController.pushViewController(vc, animated: true)
        }
        
    }
}

//MARK: School list Delegate
extension SchoolListCoordinator: SchoolListDelegate {
    
    func didTapSchool(_ school: School) {
        self.navigationController.popViewController(animated: true)
        self.goToSchoolDetailViewController(school: school)
    }
    
}

//MARK: Private Methods
extension SchoolListCoordinator {
    
    func goToSchoolDetailViewController(school: School) {
        let schoolInfoVC = DataFactory.getSchoolAdditionalInfoVC(school)
        if let vc = schoolInfoVC {
            self.navigationController.pushViewController(vc, animated: true)
        }
    }
}

