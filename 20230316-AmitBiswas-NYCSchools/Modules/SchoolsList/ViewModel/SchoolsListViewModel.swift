//
//  SchoolsListViewModel.swift
//  20200704-AmitBiswas-NYCSchools
//
//  Created by Amit Biswas on 3/16/23.
//

import Foundation


typealias SchoolsListViewModelOutput = (SchoolsListViewModel.Output) -> ()

class SchoolsListViewModel {

    private var cellViewModels = [SchoolCellListViewModel]()
    
    private var schoolsList: [School] {
        didSet {
            initializeCellViewModels()
            completionHandler?(.reloadData)
        }
    }
    
    var completionHandler: SchoolsListViewModelOutput?
    
    //MARK: - Initializer
    init(schoolsList: [School]) {
        self.schoolsList = schoolsList
    }
    
    func viewDidLoad() {
        apiCallForFetchSchoolsList()
    }
    
    enum Output {
        case reloadData
        case showLoader
        case hideLoader
        case showError(message: String)
    }

}

//MARK: - Api Call
extension SchoolsListViewModel {
    
    private func apiCallForFetchSchoolsList() {
        completionHandler?(.showLoader)
        
        Task {
            do {
                let schools: [School] = try await NetworkingManager.shared.fetchData(from: EndPoints.schoolsList)
                self.schoolsList = schools
                self.completionHandler?(.hideLoader)
                
            } catch let error {
                self.completionHandler?(.hideLoader)
                self.completionHandler?(.showError(message: error.localizedDescription))
            }
        }
    }
    
}


//MARK: - Helper Methods
extension SchoolsListViewModel {
    
    private func initializeCellViewModels() {
        cellViewModels = []
        
        for (index, school) in self.schoolsList.enumerated() {
            
            let schoolCellVM = SchoolCellListViewModel(cellIndex: index,
                                                       schoolName: school.schoolName ?? "",
                                                       schoolCity: school.city ?? "")
            self.cellViewModels.append(schoolCellVM)
        }
        
    }
    
    var rows: Int {
        return schoolsList.count
    }
    
    func cellViewModel(for row: Int) -> SchoolCellListViewModel {
        return cellViewModels[row]
    }

    func getSchoolFor(index: Int) -> School {
        return self.schoolsList[index]
    }
    
}
