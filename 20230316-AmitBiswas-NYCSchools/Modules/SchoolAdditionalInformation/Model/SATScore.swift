//
//  SATScore.swift
//  20200704-AmitBiswas-NYCSchools
//
//  Created by Amit Biswas on 3/16/23.
//

import Foundation

struct SATScore: Codable {

    var dbn: String
    var schoolName: String
    var testTakers: String
    var readingScore: String
    var mathScore: String
    var writingScore: String
        
    private enum CodingKeys: String, CodingKey {
        
        case dbn = "dbn"
        case schoolName = "school_name"
        case testTakers = "num_of_sat_test_takers"
        case readingScore = "sat_critical_reading_avg_score"
        case mathScore = "sat_math_avg_score"
        case writingScore = "sat_writing_avg_score"
    }
}
