//
//  UIStoryboard.swift
//  20200704-AmitBiswas-NYCSchools
//
//  Created by Amit Biswas on 3/16/23.
//

import UIKit


extension UIStoryboard {
    
    enum Storyboard: String {
        case main = "Main"
    }
    
    
    //MARK: - Convenience Initializers
    convenience init(storyboard: Storyboard, bundle: Bundle? = nil) {
        self.init(name: storyboard.rawValue, bundle: bundle)
        
    }
}
