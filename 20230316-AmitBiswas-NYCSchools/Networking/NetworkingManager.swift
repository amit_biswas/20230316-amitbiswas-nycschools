//
//  NetworkingManager.swift
//  20200704-AmitBiswas-NYCSchools
//
//  Created by Amit Biswas on 3/16/23.
//

import Foundation

enum NetworkError: String, Error {
    case serverError = "Server Error"
    case mappingError = "Could not able to parse response from server"
    case invalidUrl = "Invalid url"
}

final class NetworkingManager {
    
    //MARK: - Shared Instance
    static let shared = NetworkingManager()
    
    
    //MARK: - Initializers
    private init () { }
    
    func fetchData<T: Decodable>(from endPoint: EndPoints) async throws -> T {
        guard let url = URL(string: endPoint.rawValue) else {
            throw NetworkError.invalidUrl
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        let (data, response) = try await URLSession.shared.data(for: request)
        guard let httpResponse = response as? HTTPURLResponse,
              (200..<400).contains(httpResponse.statusCode) else {
            throw NetworkError.serverError
        }
        
        do {
            let decodedData = try JSONDecoder().decode(T.self, from: data)
            return decodedData
        } catch {
            throw NetworkError.mappingError
        }
    }
        
}
